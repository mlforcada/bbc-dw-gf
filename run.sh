#/bin/bash

NUMBER_OF_SENTENCES=30
NUMBER_OF_PARTICIPANTS=9

SOURCE_LANGUAGE="${1:?[Please specify the source language.]}"
TARGET_LANGUAGE="${2:?[Please specify the target language.]}"

python3 generate_one_line_files.py --sentence-num $NUMBER_OF_SENTENCES
for i in $(seq 1 $NUMBER_OF_PARTICIPANTS); do python3 wrapper.py --sl $SOURCE_LANGUAGE --tl $TARGET_LANGUAGE --target_dir /tmp/ $i "" {1..30}; done
mkdir -p output
for i in $(seq 1 $NUMBER_OF_PARTICIPANTS); do python3 merger.py --sl $SOURCE_LANGUAGE --tl $TARGET_LANGUAGE  --setid $(uuidgen) --outfile output/job$i.json /tmp/$i/*.json ; done
