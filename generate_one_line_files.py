import argparse

def errorSafeConvertStrToInt(string):
    try:
        return int(string)
    except:
        return 0


parser = argparse.ArgumentParser()
parser.add_argument("--sentence-num", help="Number of sentences required per set",
                    dest="numberOfSentences", default="30")
args = parser.parse_args()
numberOfSentences = errorSafeConvertStrToInt(args.numberOfSentences)

humanSentences = open("data/human/sentences.txt", "r").readlines()
googleSentences = open("data/google/sentences.txt", "r").readlines()
gourmetSentences = open("data/gourmet/sentences.txt", "r").readlines()
sourceSentences = open("data/source/sentences.txt", "r").readlines()


for i in range(numberOfSentences):
    human = open(f"data/human/{i+1}.txt", 'w')
    human.write(humanSentences[i])
    human.close()
    google = open(f"data/google/{i+1}.txt", 'w')
    google.write(googleSentences[i])
    google.close()
    gourmet = open(f"data/gourmet/{i+1}.txt", 'w')
    gourmet.write(gourmetSentences[i])
    gourmet.close()
    source = open(f"data/source/{i+1}.txt", 'w')
    source.write(sourceSentences[i])
    source.close()