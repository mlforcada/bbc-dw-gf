# bbc-dw-gf

Gap-filling software for BBC and DW as part of the [GoURMET](http://www.gourmet-project.eu) project.

## Running the script

The following scripts can be run and the packages required installed within a [virtual env](https://docs.python.org/3/tutorial/venv.html) using python 3 and [pip](https://pypi.org/project/pip/). The virtual env removes the need to install packages locally.

```bash
python3 -m venv .
source bin/activate
pip install -r requirements.txt
```

If not using virtual env. The packages required can be installed using:

```bash
pip install -r requirements.txt
```

### Downloading Punkt Tokenizer Models

To run the script the Punkt Tokenizer Models must be downloaded this can be done using the Python REPL:

```
python3                                                                                                                                                             
Python 3.7.7 (default, Mar 10 2020, 15:43:33)
[Clang 11.0.0 (clang-1100.0.33.17)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import nltk
>>> nltk.download('punkt')
>>> quit()
```

## run.sh

There is a run (`./run.sh`) script in the root directory that takes the source and target language codes and generates 9 jobs

e.g.

```
./run.sh gu en
```

Script to generate 9 jobs in the `output` folder. Assumes:
1. The directories `gourmet`, `google`, `source` and `human` in the `data` directory all contain a file `sentences.txt`
2. The `sentences.txt` file contains 30 sentences
3. The sentences are translations of the sentences in the `source` directory copy of `sentences.txt`.

The 9 jobs will contain a mixture of segments where there is no hint text, or the hint text is from Google translate, or the hint text is translated by GoURMET models.

## generate_one_line_files.py

Helper script. Assumes the presence of a `sentences.txt` file in the `gourmet`, `google`, `source` and `human` directories where each line of the file is a single sentence and that there is a relationship between the sentences in each `sentences.txt` file i.e. they are all the same sentence as in the `source` file but translated. Creates the single sentence files required by the `wrapper.py` script.

## prepare_one.py

Takes three files: a reference text file, a source text file and a hint text file containing a single line and produces a JSON results file.
In this project, both the reference and the hint text are a single sentence.

This is meant to be called by a wrapper, but may be used directly. The idea is to run it from another script (see wrapper.py below) 
to generate jobs from the files in which each segment is in a  different configuration.

If --no_hint is given, the hint text is ignored.

One can set the percentage of gaps (e.g.  --percentage 20 for 20%), the source language (--sl), the target language (--tl) which will be used to label the JSON file. 
A document id (--docid) is also provided.

`Please note`: there are remnants of previous code that used entropy to fill gaps; the code could be cleaned to remove them.
If a list of stopwords is available for the language in question from package stop_words, then holes are never punched where stopwords are. 

The switch --adjacent_gaps_not_ok avoids punching gaps which are adjacents or just separated by stopwords.

The switch --system labels the system used.



```
usage: prepare_one.py [-h] [--percentage PERCENTAGE] [-v]
                      [--include_stopwords] [--include_punctuation]
                      [--no_hint] [--setid SETID] [--docid DOCID] [--sl SL]
                      [--tl TL] [--system SYSTEM] [--adjacent_gaps_not_ok]
                      reftextfile hinttextfile resultfile position
```

When called like
```
python prepare_one.py --percentage 20   --setid myset --docid mydoc --sl bg --tl en --system google --adjacent_gaps_not_ok human/2.txt google/2.txt result.json 
```
it produces an JSON file like

```
{
  "id": "myset",
  "sourceLanguage": "bg",
  "targetLanguage": "en",
  "segments": [
    {
     "id": "editme::mydoc",
     "translationSystem": "google",
     "gapDensity": "20.0",
     "context": "-doc",
     "entropyMode": "-ent",
     "source": "Това означава, че лесбийските двойки могат да отнемат биологични деца, като се аргументират, че насърчават „определени ценности“ със своя начин на живот.",
     "hint": "This means that lesbian couples can take away biological children, arguing that they are promoting "certain values" with their lifestyle.",
     "problem": " This means that lesbian couples could even have their biological children taken { } because , through their lifestyle { } , they propagate `` { } values . ''",
     "correctAnswers": ["away", "choices", "certain"]
    }
  ]
}

```
When called like 
```
 python prepare_one.py --percentage 20   --setid myset --docid mydoc --sl bg --tl en --no_hint --adjacent_gaps_not_ok human/2.txt google/2.txt result.json
```
it produces an JSON file like
```
{
  "id": "myset",
  "sourceLanguage": "bg",
  "targetLanguage": "en",
  "segments": [
    {
     "id": "editme::mydoc",
     "translationSystem": "NONE",
     "gapDensity": "20.0",
     "context": "-doc",
     "entropyMode": "-ent",
     "source": "Dummy text, not shown",
     "hint": "[Take your best guess]",
     "problem": " This means that lesbian couples could even have their biological children taken { } because , through their lifestyle { } , they propagate `` { } values . ''",
     "correctAnswers": ["away", "choices", "certain"]
    }
  ]
}

```

The JSON format can easily be customized for easier merging.


These "set" JSON files containing a single segment may be merged into larger files by removing border lines and concatenating (see merger.py below).


## wrapper.py

This program receives an informant number, the directory where all problem files are kept (in directories such as human, google, gourmet, etc., 
and the names of files to get from each of these directories) and generates a set of files in directory /tmp/i where i is the informant number set of one-problem JSON
files that will later be merged by merger.py.

The code may be improved massively, as it has a number of hard-wired parameters such as the names of the directories (named after the systems).


```
usage: wrapper.py [-h] [-v] [--dry_run] [--sl SL] [--tl TL]
                  [--target_dir TARGET_DIRECTORY]
                  informant documents_root problemfiles [problemfiles ...]

positional arguments:
  informant             Informant number
  documents_root        root of document files
  problemfiles          names of problems to process

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         Verbose Mode
  --dry_run             Dry run
  --sl SL               Source language
  --tl TL               Source language
  --target_dir TARGET_DIRECTORY
                        Target directory
```

Example:
```
python wrapper.py --sl bg --tl en --target_dir /tmp/ 3 "" {1..30}
```
Here the informant is number 3, but it can be called in a loop for 9 informers 0 to 8
```
for i in {0..8}; do python wrapper.py --sl bg --tl en --target_dir /tmp/ $i "" {1..30}; done

```
This creates a series of directories 0, 1, ..., 9 where the JSON files for each job are stored.


# merger.py

Concatenates a series of JSON tasks generated by prepare_one.py into a single JSON file (specified by --output) by wrapping it in a set object, 
ready to be ingested by the GoURMET gap-filling softwre. The job may be given a set identifier (--setid).

JSON files concatenated should each one contain a single segment (as generated by prepare_one.py).

Usage:

```
usage: merger.py [-h] [--setid SETID] [--outfile OUTFILE] [--sl SL] [--tl TL]
                 mergefile [mergefile ...]

positional arguments:
  mergefile          files to merge

optional arguments:
  -h, --help         show this help message and exit
  --setid SETID      New set identifier
  --outfile OUTFILE  Output file
  --sl SL            Source language
  --tl TL            Source language

```
This would usually be called after wrapper.py in a loop like the one, to generate the job files to be uploaded to the GoURMET gap-filling interface:
```
for i in {0..8}; do python merger.py --sl bg --tl en--setid $i --outfile /tmp/job$i.json /tmp/$i/*.json ; done

```



