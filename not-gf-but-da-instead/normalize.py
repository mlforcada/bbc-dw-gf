#!/usr/bin/python3 -B

import csv
import sys

with open(sys.argv[1]) as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        collist = []
        for col in row :
            col = ''.join(col.splitlines())
            col = '\"' + col + '\"'
            collist.append(col)
        print(','.join(collist))
