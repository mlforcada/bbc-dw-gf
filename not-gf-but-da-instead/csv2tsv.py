#!/usr/bin/python3 -B
# This reads a CSV file with newlines and generates a TSV file without them


import csv
import sys

with open(sys.argv[1]) as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        collist = []
        for col in row :
            col = ''.join(col.splitlines())
            collist.append(col)
        print('\t'.join(collist))
