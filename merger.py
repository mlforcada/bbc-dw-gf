#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

import argparse
import string

parser=argparse.ArgumentParser()
parser.add_argument("--setid", dest="setid", help="New set identifier", default="defaultsetid")
parser.add_argument("--outfile", dest="outfile", help="Output file")
parser.add_argument("--sl", dest="sl", help="Source language")
parser.add_argument("--tl", dest="tl", help="Source language")
parser.add_argument("mergefiles", metavar="mergefile", help="files to merge", nargs='+')
args=parser.parse_args()

nfiles=len(args.mergefiles)

out=open(args.outfile,"w")
out.write('{\n')
out.write(' "id": "{0}"\n,'.format(args.setid))
out.write(' "sourceLanguage": "{0}",\n'.format(args.sl))
out.write(' "targetLanguage": "{0}",\n'.format(args.tl))
out.write(' "segments": [\n')


segcounter=0
nfiles=len(args.mergefiles);
for _k,_file in enumerate(args.mergefiles) :
	_lines=open(_file).readlines()
	segcounter=segcounter+1 #each file has a single segment; won't work otherwise
	for i in range(5,len(_lines)-2) :
		out.write(_lines[i].replace("editme::", str(segcounter) + ":inf" + args.setid + ":"))
	if _k < nfiles-1 :
		out.write('     ,\n')
out.write('  ]\n')
out.write('}\n')
out.close()
   
   



