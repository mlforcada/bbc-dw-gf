# Still chasing a bug
# Getting uglier and uglier
file=$1 
jpg=$2
# file is the first argument in TSV file with this structure
declare -a f # array
f[1]="Gap Id"
f[2]="Evaluator Id"
f[3]="Sentence Id"
f[4]="Source Language"
f[5]="Number of Gaps"
f[6]="Hint Type"
f[7]="Missing Word"
f[8]="Answer Given"
f[9]="Auto Answer Match"
f[10]="Manual Answer Match"
f[11]="Sentence Submission Time (ms)"
f[12]="Mean Time per Gap (ms)"
f[13]="Sentence as seen by evaluator"
f[14]="Hint sentence as seen by evaluator"
f[15]="Sentence without gaps"
f[16]="Source"
# echo ${f[11]}
for i in $(seq 1 16)
do
   model=${f[$i]}
   test=$(head -1 $file | cut -f$i | tr -d '[\012\015]')
   # the -d part is needed because the last field contains newlines, etc.
   if [ "$model" != "$test" ]
   then
      echo $i "Error in expected header" # when the file format is not exactly OK
      break
   fi
   echo "File format looks OK"
done


num=0
den=0

printf "*** Raw statistics ***\n"
printf "Number of data points: "
# tail +2 removes the header line
nlin=$(tail +2 $file | wc -l )
echo $nlin

printf "Number of unique gap ids: "
nug=$(tail +2 $file | cut -f 1 | sort | uniq | wc -l)
echo $nug

printf "Number of unique sentence ids: "
nus=$(tail +2 $file | cut -f 3 | sort | uniq | wc -l)
echo $nus

avgps=$(echo $nug / $nus | bc -l)
printf "Average number of gaps per sentence: " 
echo $avgps

listev=$(tail +2 $file | cut -f 2 | sort | uniq)  
numev=$(tail +2 $file | cut -f 2 | sort | uniq | wc -l)

printf "List of evaluators (total %s): \n" $numev
 for ev in $listev 
do
   printf "%s: " $ev
   printf "%s gaps " $(tail +2 $file | grep $ev | cut -f 1 | sort | uniq | wc -l)
   printf "%s sentences " $(tail +2 $file | grep $ev | cut -f 3 | sort | uniq | wc -l)
   printf "\n"
done

for nev in $(seq 1 10)
do
n=$(tail +2 $file | cut -f 1 | sort | uniq -c | grep $nev" " | wc -l)
nn=$(tail +2 $file | cut -f 1,6 | sort | uniq -c | grep $nev" " | wc -l)
if [ "$n" -ne "$nn" ] 
then
	echo "Possible problem with gap-configuration Ids"
	echo "Please check if two gap IDs correspon to different gaps".
	exit
fi
if [ "$n" -ne 0 ]
then
printf "Number of gap-configuration ids with %d evaluations: %s\n" $nev $n 
num=$[ $num + $nev * $n ]
den=$[ $den + $n ]
fi
done
avevpgc=$( echo $num / $den | bc -l )
printf "Average evaluations per gap-configuration: %s" 
echo $avevpgc

if [ "$den" -eq "$nug" ] 
then
printf "Test #1 OK\n"
else
printf "Test #1 FAILED\n" 
fi

printf "Number of gaps processed per evaluator: \n"
tail +2 $file | cut -f 2 | sort | uniq -c  

printf "*** Sanity checks***\n"
remove=$(tail +2 $file | cut -f 1,2,6,7 | sort | uniq -c | sort -nr | grep "[23456789] " | cut -f2 | sort | uniq | tr '\012' ' ' | awk '{printf "("; for (i=1;i<=NF-1;i++) printf "%s|",$i; printf "%s)",$NF}')
printf "Evaluators that have processed the same gap more than once:\n"
if [ -z "$remove" ]
then
printf "NONE\n"
nlin=$(tail +2 $file | wc -l )
avgps=$(echo $nug / $nus | bc -l)
else 
echo $remove 
printf "Their work will be removed from the ensuing calculations\n"
printf "**** after removing duplicates ****\n"
nlin=$(tail +2 $file | egrep -v $remove | wc -l )
echo $nlin
avgps=$(echo $nug / $nus | bc -l)
printf "Average number of gaps per sentence: " 
echo $avgps
fi


printf "List of evaluators:"
if [ -z "$remove" ]
then
listev=$(tail +2 $file | cut -f 2 | sort | uniq)  
numev=$(tail +2 $file| cut -f 2 | sort | uniq | wc -l)
else
listev=$(tail +2 $file | egrep -v $remove | cut -f 2 | sort | uniq)  
numev=$(tail +2 $file| egrep -v $remove | cut -f 2 | sort | uniq | wc -l)
fi
for ev in $listev 
do
   printf "%s: " $ev
done
printf "\n(total %s)\n" $numev



num=0
den=0
for nev in $(seq 1 4)
do
if [ -z "$remove" ]
then
n=$(tail +2 $file | cut -f 1 | sort | uniq -c | grep $nev" " | wc -l)
else
n=$(tail +2 $file | egrep -v $remove |cut -f 1 | sort | uniq -c | grep $nev" " | wc -l)
fi

if [ "$n" -ne 0 ]
then
printf "Number of gap-configurations with %d evaluations: " $nev 
echo $n
num=$[ $num + $nev * $n ]
den=$[ $den + $n ]
fi
done
avevpgc=$( echo $num / $den | bc -l )
printf "Average evaluations per gap-configuration: %s" 
echo $avevpgc

printf "Analysis by hint type:\n"
if [ -z "$remove" ]
then
    cat $file | tail +2 | cut -f 6 | sort | uniq -c | sort -nr
else
    egrep -v $remove $file | tail +2 | cut -f 6 | sort | uniq -c | sort -nr
fi


printf "List of systems: "
if [ -z "$remove" ]
then
listsys=$(tail +2 $file |  cut -f 6 | sort | uniq)
else  
listsys=$(tail +2 $file | egrep -v $remove | cut -f 6 | sort | uniq)  
fi
echo $listsys

printf "Success rates per system: (exact)\n"
for i in $listsys
   do printf "%s:" $i; 
     if [ -z "$remove" ]
     then
     cat $file | tail +2 | cut -f 6,9 | grep $i | awk '$2=="YES" {yes++} $2=="NO" {no++} END{printf "%7.2f%\n",100*yes/(yes+no)}';      
     else
     egrep -v $remove $file | tail +2 | cut -f 6,9 | grep $i | awk '$2=="YES" {yes++} $2=="NO" {no++} END{printf "%7.2f%\n",100*yes/(yes+no)}'; 
     fi
   done
   
printf "Success rates per system: (manual)\n"
for i in $listsys; 
   do printf "%s:" $i; 
   if [ -z "$remove" ]
   then
     cat $file | tail +2 | cut -f 6,10 | grep $i | awk '$2=="YES" {yes++} $2=="NO" {no++} END{printf "%7.2f%\n",100*yes/(yes+no)}'; 
   else
     egrep -v $remove $file | tail +2 | cut -f 6,10 | grep $i | awk '$2=="YES" {yes++} $2=="NO" {no++} END{printf "%7.2f%\n",100*yes/(yes+no)}'; 
   fi
   done

# Make a list of systems
declare -a list
system=($listsys) # Do I need this
# Later


# This assumes this order: NONE google gourmet # not general - to be improved
printf "Computing boxplot\n"
# this needs to be improved, hey
datafile=$(mktemp)
if [ -z "$remove" ]
then
tail +2 $file | awk '{FS="\t"} {all[$6"\t"$2]++} $9=="YES" {yes[$6"\t"$2]++} $9=="NO" {no[$6"\t"$2]++} END{for (p in all) {print p, yes[p]/all[p]}}' | sort -k 1 | awk '{evals[$2]++} $1=="NONE" {none[$2]=$3} $1=="google" {google[$2]=$3} $1=="gourmet" {gourmet[$2]=$3} END{for (e in evals) print e, none[e], google[e], gourmet[e]}' | sort -k 1 >$datafile
else
tail +2 $file | egrep -v $remove| awk '{FS="\t"} {all[$6"\t"$2]++} $9=="YES" {yes[$6"\t"$2]++} $9=="NO" {no[$6"\t"$2]++} END{for (p in all) {print p, yes[p]/all[p]}}' | sort -k 1 | awk '{evals[$2]++} $1=="NONE" {none[$2]=$3} $1=="google" {google[$2]=$3} $1=="gourmet" {gourmet[$2]=$3} END{for (e in evals) print e, none[e], google[e], gourmet[e]}' | sort -k 1 >$datafile
fi

echo $listsys
cat $datafile
# Currently plots 3 systems
# Will work on it to make it more general
# gnuplot boxplot.gnuplot
plotfile=$jpg
gnuplot --persist <<-EOFMarker
set xtics font ",20"
set ytics font ",20"
set style data boxplot
set style boxplot outliers pointtype 7
set boxwidth  0.5
set pointsize 0.5
unset key
set border 2
set yrange [0:1]
set xrange [0:2]
set xtics nomirror
set ytics nomirror
set style fill solid 0.5 border -1
set xtics ("NONE" 0.4, "Google" 1.0, "GoURMET" 1.6) scale 0.0
set terminal jpeg
set output "`echo $plotfile`"
plot "`echo $datafile`" using (0.4):2, '' using (1.0):3, '' using(1.6):4
EOFMarker
