# generate a list of substitutions to be edited and introduced in the shell
file=$1 # TSV file to be processed
remove=$2 # list of banned evaluators in double quotes and parentheses as this: 
          # (DW_4_SW|DW_5_SW)
minfreq=$3 # minimum frequency for listing
if [ "$remove" = "" ]
then
tail +2 $file | cut -f 7,8,9 | awk '$1!=$2' | sort | uniq -c | sort -nr | awk -v f=$minfreq '$1>=f {print $1,$2,$3}'
else
tail +2 $file | egrep -v $remove | cut -f 7,8,9 | awk '$1!=$2' | sort | uniq -c | sort -nr | awk -v f=$minfreq '$1>=f {print $1, $2, $3}'
fi
