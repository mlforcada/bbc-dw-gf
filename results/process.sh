# Still chasing a small bug
# Getting uglier and uglier
file=$1 # TSV file with data in the format below
jpg=$2 # Output file (JPG boxplot)
# Catch error and print usage message
if [ "$#" -ne 2 ]; then
    printf "Usage: " 
    printf $0 
    printf " TSV_file JPG_file\n"
    exit 1
fi
declare -a f # array
f[1]="Gap Id"
f[2]="Evaluator Id"
f[3]="Sentence Id"
f[4]="Source Language"
f[5]="Number of Gaps"
f[6]="Hint Type"
f[7]="Missing Word"
f[8]="Answer Given"
f[9]="Auto Answer Match"
f[10]="Manual Answer Match"
f[11]="Sentence Submission Time (ms)"
f[12]="Mean Time per Gap (ms)"
f[13]="Sentence as seen by evaluator"
f[14]="Hint sentence as seen by evaluator"
f[15]="Sentence without gaps"
f[16]="Source"

# Sanity checks
# Sanity check #1: Check header in first line of TSV against expected labels
for i in $(seq 1 16)
do
   model=${f[$i]}
   test=$(head -1 $file | cut -f$i | tr -d '[\012\015]')
#   echo "<"${model}">" ${#model}
#   echo "<"$test">" ${#test}
#   echo "<"${model:0:1}">"
#   echo "<"${test:0:1}">"
#   printf "%d\n" "${test:2:1}"
#   if [ "${model:1:1}" != "${test:1:1}" ]
#   then
#      echo "bang"
#      exit 1
#   fi
   # the -d part is needed because the last field contains newlines, etc.
   if [ "$model" != "$test" ]
   then
      echo $i "Error in expected header" # when the file format is not exactly OK
      exit 1  
   fi
done 

# Check no evaluator has seen the same problem twice
# Checks if a gap id (1)-evaluator(2)-hint(3)-missing word(7) quartet appears more than once; in that case, the evaluator name is 
# added to the string remove
# and will not be taken into account for the remaining analysis
remove=$(tail +2 $file | cut -f 1,2,6,7 | sort | uniq -c | sort -nr | grep "[23456789] " | cut -f2 | sort | uniq | tr '\012' ' ' | awk '{printf "("; for (i=1;i<=NF-1;i++) printf "%s|",$i; printf "%s)",$NF}')

filtered_TSV=$(mktemp)
if [ -z "$remove" ]
then
   tail +2 $file >$filtered_TSV
else
   printf "Evaluators that have processed the same gap more than once:\n"
   echo $remove 
   printf "Their work will be removed from the ensuing calculations \n"
   tail +2 $file | grep -v $remove >$filtered_TSV
fi

printf "*** Raw statistics ***\n"
# Lines in file except first one
printf "Number of data points (lines in file): "
# tail +2 removes the header line
nlin=$(cat $filtered_TSV | wc -l )
echo $nlin


# Counts looking at the data and ignoring the IDs
# Number of unique gaps: (16=source sentence and 7=missing word in target sentence)
printf "Number of unique gaps "
nug=$(cat $filtered_TSV | cut -f 7,16 | sort | uniq | wc -l)
echo $nug

# Number of unique sentences (16=source sentence)
printf "Number of unique sentence ids: "
nus=$(cat $filtered_TSV | cut -f 16 | sort | uniq | wc -l)
echo $nus

# Number of unique gap-hint combinations (16=source sentence 
# 7=missing word in target sentence, and 6=hint)
printf "Number of unique gap-hint combinations: "
nugh=$(cat $filtered_TSV | cut -f 6,7,16 | sort | uniq | wc -l)
echo $nugh

# Number of gaps per sentence
avgps=$(echo $nug / $nus | bc -l)
printf "Average number of gaps per sentence: " 
echo $avgps

# Number of evaluations per gap-hint combinations
avegph=$(echo $nlin/ $nugh | bc -l)
printf "Average number of evaluators per gap-hint combination: " 
echo $avegph

# List of evaluators and their number (2=name of evaluator)
listev=$(cat $filtered_TSV | cut -f 2 | sort | uniq)  
numev=$(cat $filtered_TSV | cut -f 2 | sort | uniq | wc -l)
printf "List of evaluators (total %s): \n" $numev
# For each evaluator, compute how many gap hint evaluations and sentences
for ev in $listev 
do
   printf "%s: " $ev
   printf "%s gap-hint configurations " $(cat $filtered_TSV | grep $ev | cut -f 6,7,16 | sort | uniq | wc -l)
   printf "%s sentences " $(cat $filtered_TSV | grep $ev | cut -f 16 | sort | uniq | wc -l)
   printf "\n"
done

# Computing the number of gap-hint configurations with a certain number of evaluations
# This may be buggy because of a problem with the grep regex below
num=0
den=0
for nev in $(seq 1 20) # I had to increase this as some gaps had a large number of evaluations
do
    nughn=$(cat $filtered_TSV | cut -f 6,7,16 | sort | uniq -c | grep "^[ ]\+"$nev"[ ]" | wc -l)
    if [ "$nughn" -ne 0 ]
    then
        printf "Number of gap-configuration ids with %d evaluations: %s\n" $nev $nughn 
        num=$[ $num + $nev * $nughn ]
        den=$[ $den + $nughn ]
    fi
done
avevpgc=$( echo $num / $den | bc -l )
printf "Average evaluations per gap-configuration: %s" 
echo $avevpgc

# Another sanity check/ small checksum 
if [ "$den" -ne "$nugh" ] 
then
  printf "Internal test #1 FAILED\n" 
  printf "Number of gap-confguration ids computed in two different ways did not match\n"
  printf "den=%d\n" $den
  printf "nugh=%d\n" $nugh
  # exit 1
fi

# Number of gaps processed by evaluator
printf "Number of gaps processed per evaluator: \n"
cat $filtered_TSV | cut -f 2 | sort | uniq -c  

# How many hints of each type
printf "Analysis by hint type:\n"
cat $filtered_TSV | cut -f 6 | sort | uniq -c | sort -nr

# List of hint types:
printf "List of hint types: "
listsys=$(cat $filtered_TSV | cut -f 6 | sort | uniq)  
echo $listsys

# Success rates per system
printf "Success rates per system: (exact)\n"
for i in $listsys
   do printf "%s:" $i;
   # Check field 9 and count number of YES and NO 
      cat $filtered_TSV | cut -f 6,9 | grep $i | awk '$2=="YES" {yes++} $2=="NO" {no++} END{printf "%7.2f%\n",100*yes/(yes+no)}'; 
   done
   
printf "Success rates per system: (manual)\n"
   for i in $listsys; 
      do printf "%s:" $i; 
      # Check field 9 and count number of yes and NO
        cat $filtered_TSV | cut -f 6,10 | grep $i | awk '$2=="YES" {yes++} $2=="NO" {no++} END{printf "%7.2f%\n",100*yes/(yes+no)}'; 
      done

# Make a list of systems
declare -a list
system=($listsys) # Do I need this
# Later

# This assumes this order: NONE google gourmet # not general - to be improved
printf "Computing boxplot\n"
datafile=$(mktemp)
cat $filtered_TSV | awk '{FS="\t"} {all[$6"\t"$2]++} $9=="YES" {yes[$6"\t"$2]++} $9=="NO" {no[$6"\t"$2]++} END{for (p in all) {print p, yes[p]/all[p]}}' | sort -k 1 | awk '{evals[$2]++} $1=="NONE" {none[$2]=$3} $1=="google" {google[$2]=$3} $1=="gourmet" {gourmet[$2]=$3} END{for (e in evals) print e, none[e], google[e], gourmet[e]}' | sort -k 1 >$datafile

echo $listsys
cat $datafile
# Currently plots 3 systems
# Will work on it to make it more general
# gnuplot boxplot.gnuplot
plotfile=$jpg
gnuplot --persist <<-EOFMarker
set xtics font ",20"
set ytics font ",20"
set style data boxplot
set style boxplot outliers pointtype 7
set boxwidth  0.5
set pointsize 0.5
unset key
set border 2
set yrange [0:1]
set xrange [0:2]
set xtics nomirror
set ytics nomirror
set style fill solid 0.5 border -1
set xtics ("NONE" 0.4, "Google" 1.0, "GoURMET" 1.6) scale 0.0
set terminal jpeg
set output "`echo $plotfile`"
plot "`echo $datafile`" using (0.4):2, '' using (1.0):3, '' using(1.6):4
EOFMarker
