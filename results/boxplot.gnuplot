		set xtics font ",20"
		set ytics font ",20"
		set style data boxplot
		set style boxplot outliers pointtype 7
		set boxwidth  0.5
		set pointsize 0.5
		unset key
		set border 2
		set yrange [0:1]
		set xrange [0:2]
		set xtics nomirror
		set ytics nomirror
		set style fill solid 0.5 border -1
		set xtics ("NONE" 0.4, "Google" 1.0, "GoURMET" 1.6) scale 0.0
		set terminal jpeg
		set output "boxplot.jpg"
		plot 'data.txt' using (0.4):2, '' using (1.0):3, '' using(1.6):4

